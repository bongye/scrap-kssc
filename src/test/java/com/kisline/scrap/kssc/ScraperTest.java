package com.kisline.scrap.kssc;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.kisline.scrap.kssc.vo.Key;

public class ScraperTest {

  @Test
  public void testGetDefaultKeys() {
    try {
      Key key = new Key();
      key.setupDefault();
      Scraper scraper = new Scraper();
      List<Key> keys = scraper.getKeys(key);
      Assert.assertEquals(21, keys.size());
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testGetGuide() {
    try {
      Scraper scraper = new Scraper();
      Key key = new Key();
      key.strCategoryNameCode = "001";
      key.strCategoryCode = "01";
      key.strCategoryDegree = "09";
      key.strCategoryCodeName = "0";
      key.categoryMenu = "007";

      scraper.getGuide(key);
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testExecute() {
    try {
      Scraper scraper = new Scraper();
      scraper.execute();
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }
}
