package com.kisline.scrap.kssc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.kisline.scrap.kssc.vo.Guide;
import com.kisline.scrap.kssc.vo.Key;

public class Scraper {
  private final String domain = "https://kssc.kostat.go.kr:8443";

  public void execute() throws IOException {
    try {
      // 초기 트리메뉴 key
      Key key = new Key();
      key.setupDefault();
      List<Key> keys = getKeys(key);

      // 차례대로 나오기 위해 stack에 거꾸로 넣는다.
      Collections.reverse(keys);
      Stack<Key> stack = new Stack<>();
      stack.addAll(keys);

      List<Guide> guides = new ArrayList<>();
      while (!stack.isEmpty()) {
        key = stack.pop();
        // 하위 메뉴를 stack에 추가
        List<Key> subKeys = getKeys(key);
        Collections.reverse(subKeys);
        stack.addAll(subKeys);

        // 해당 메뉴는 추출해서 guides에 넣음
        Guide guide = getGuide(key);
        System.out.println(guide);
        guides.add(guide);
      }
    } catch (Exception e) {
      throw new IOException(e);
    }
  }


  public List<Key> getKeys(Key key) throws IOException, ParseException {
    String url = domain + "/ksscNew_web/kssc/common/ClassificationContentMainTreeList.do";
    Map<String, String> parameters = new HashMap<>();
    parameters.put("strCategoryNameCode", key.strCategoryNameCode);
    parameters.put("strCategoryDegree", key.strCategoryDegree);
    parameters.put("root", key.strCategoryCode);
    Connection connection = Jsoup.connect(url).validateTLSCertificates(false).data(parameters);
    Response response = connection.execute();

    JSONParser jsonParser = new JSONParser();
    JSONArray jsonArray = (JSONArray) jsonParser.parse(response.body());
    return parseKeys(jsonArray);
  }

  public Guide getGuide(Key key) throws IOException, ParseException {

    String url = domain + "/ksscNew_web/kssc/common/ClassificationContentMainTreeListView.do";
    Map<String, String> parameters = new HashMap<>();
    parameters.put("strCategoryNameCode", key.strCategoryNameCode);
    parameters.put("strCategoryCode", key.strCategoryCode);
    parameters.put("strCategoryDegree", key.strCategoryDegree);
    parameters.put("strCategoryCodeName", key.strCategoryCodeName);
    parameters.put("categoryMenu", key.categoryMenu);

    Connection connection = Jsoup.connect(url).validateTLSCertificates(false).data(parameters);
    Document document = connection.get();
    return parseGuide(document);
  }

  /**
   * parse tree menu json array
   * 
   * ex) [{'hasChildren':true, 'text': .., 'id': 'A'}, {..}, {..}]
   * 
   * @param jsonArray
   * @return
   */
  private List<Key> parseKeys(JSONArray jsonArray) {
    List<Key> keys = new ArrayList<>();

    for (int i = 0; i < jsonArray.size(); i++) {
      JSONObject jsonObject = (JSONObject) jsonArray.get(i);
      String text = (String) jsonObject.get("text");
      Document document = Jsoup.parse(text);

      String onclick = document.select("tr").first().attr("onclick");
      String tmp = onclick.substring(onclick.indexOf("(") + 1, onclick.lastIndexOf(")"));

      String[] tokens = tmp.split(",");

      Key key = new Key();
      key.strCategoryNameCode = tokens[0].replaceAll("'", "");
      key.strCategoryCode = tokens[1].replaceAll("'", "");
      key.strCategoryDegree = tokens[2].replaceAll("'", "");
      key.strCategoryCodeName = tokens[3].replaceAll("'", "");
      key.categoryMenu = tokens[4].replaceAll("'", "");
      keys.add(key);
    }
    return keys;
  }

  private Guide parseGuide(Document document) {
    Guide guide = new Guide();
    Elements elements = document.select("td");
    guide.order = elements.get(0).text();
    guide.categoryCode = elements.get(1).text();
    guide.categoryName = elements.get(2).text();
    guide.description = elements.get(3).text();
    guide.tags = elements.get(4).text();
    return guide;
  }
}
