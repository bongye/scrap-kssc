package com.kisline.scrap.kssc.vo;

/**
 * 좌측 트리메뉴에서 각각의 분류에 접근하기 위한 parameter
 * 
 * @author wonhyung
 */
public class Key {
  public String strCategoryNameCode;
  public String strCategoryCode;
  public String strCategoryDegree;
  public String strCategoryCodeName;
  public String categoryMenu;

  /**
   * 처음 트리메뉴를 가져오기 위한 파라미터 setup
   * 
   */
  public void setupDefault() {
    this.strCategoryNameCode = "001";
    this.strCategoryDegree = "09";
    this.strCategoryCode = "source";
  }

  @Override
  public String toString() {
    return "Key [strCategoryNameCode=" + strCategoryNameCode + ", strCategoryCode="
        + strCategoryCode + ", strCategoryDegree=" + strCategoryDegree + ", strCategoryCodeName="
        + strCategoryCodeName + ", categoryMenu=" + categoryMenu + "]";
  }
}
