package com.kisline.scrap.kssc.vo;

/**
 * 스크래핑 결과
 * 
 * @author wonhyung
 */
public class Guide {
  // 차수
  public String order;

  // 분류코드
  public String categoryCode;

  // 분류명
  public String categoryName;

  // 설명
  public String description;

  // 색인어
  public String tags;

  @Override
  public String toString() {
    return "Guide [order=" + order + ", categoryCode=" + categoryCode + ", categoryName="
        + categoryName + ", description=" + description + ", tags=" + tags + "]";
  }
}
